package com.companies.companiesapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CompanyController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/greeting")
    public String getGreeting() {
        return "Tere!!!";
    }

    @GetMapping("/companies")
    public List<Company> getCompanies() {
        List<Company> companies = jdbcTemplate.query("select * from company", (row, count) -> {
                    int id = row.getInt("id");
                    String companyName = row.getString("name");
                    String companyLogo = row.getString("logo");

                    Company company = new Company();
                    company.setId(id);
                    company.setName(companyName);
                    company.setLogo(companyLogo);

                    return company;
                }
        );

        return companies;
    }

    @GetMapping("/company/{id}")
    public Company getCompany(@PathVariable int id) {
        Company result = jdbcTemplate.queryForObject("select * from company where id = ?", new Object[] { id }, (row, count) -> {
            int companyId = row.getInt("id");
            String companyName = row.getString("name");
            String companyLogo = row.getString("logo");

            Company company = new Company();
            company.setId(companyId);
            company.setName(companyName);
            company.setLogo(companyLogo);

            return company;
        });

        return result;
    }

    @DeleteMapping("/company/{id}")
    public void deleteCompany(@PathVariable int id) {
        jdbcTemplate.update("delete from company where id = ?", id);
    }

    @PostMapping("/company")
    public void addCompany(@RequestBody Company company) {
        jdbcTemplate.update("insert into company (name, logo) values (?, ?)", company.getName(), company.getLogo());
    }

    @PostMapping("/companies")
    public void addCompanies(@RequestBody Company[] companies) {
        for (Company company : companies) {
            addCompany(company);
        }
    }

    @PutMapping("/company")
    public void editCompany(@RequestBody Company company) {
        jdbcTemplate.update("update company set name = ?, logo = ? where id = ?",
                company.getName(), company.getLogo(), company.getId());
    }
}
